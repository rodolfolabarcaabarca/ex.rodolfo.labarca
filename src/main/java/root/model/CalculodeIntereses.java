/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model;

import java.text.DecimalFormat;

/**
 *
 * @author rlabarca
 */
public class CalculodeIntereses {
    public String intereses(String capital, String intereses, String anhos){
        
        float lcapital = Float.parseFloat(capital);
        float lintereses = Float.parseFloat(intereses); 
        float lanhos = Float.parseFloat(anhos); 
        
        float interesTotal = lcapital * (lintereses / 100) * lanhos;
        DecimalFormat sindecimales = new DecimalFormat("#");
        String interes = String.valueOf(sindecimales.format(interesTotal)); 
        
        return interes;
        
    }
}
