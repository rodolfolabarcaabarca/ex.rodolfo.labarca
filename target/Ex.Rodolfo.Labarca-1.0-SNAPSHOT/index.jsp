<%-- 
    Document   : index
    Created on : 29-03-2020, 16:44:44
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculo de Intereses</title>
        <link rel="stylesheet" type="text/css" href="view.css" media="all">
        <script type="text/javascript" src="view.js"></script>
    </head>
    <body id="main_body">
        <%-- --%>
        <img id="top" src="top.png" alt="">
	<div id="form_container">
	<h1><a>Calculo de Intereses</a></h1>
            <form name="form" action="controller" method="POST">
                <div class="form_description">
                    <h2>Calculo de Intereses</h2>
                    <p>Ingrese los datos para realizar el calculo de intereses</p>
		</div>						
		<ul >
                    <li id="li_1" >
                        <label class="description" for="element_1">Capital </label>
                        <div>
                            <input id="element_1" name="capital" class="element text medium" type="number" maxlength="200" value=""/> 
                        </div>
                        <p class="guidelines" id="guide_1">
                        <small>Ingrese Capital a Financias</small></p> 
                    </li>		
                    <li id="li_2" >
                        <label class="description" for="element_2">Interes Anual (%)</label>
                        <div>
                            <input id="element_2" name="intereses" class="element text small" type="number" maxlength="3" value=""/> 
                        </div>
                        <p class="guidelines" id="guide_2">
                        <small>Ingrese el Interes Anual</small></p> 
                    </li>
                    <li id="li_2" >
                        <label class="description" for="element_2">Cantidad de Años</label>
                        <div>
                            <input id="element_2" name="anhos" class="element text small" type="number" maxlength="3" value=""/> 
                        </div>
                        <p class="guidelines" id="guide_2">
                        <small>Ingrese La Cantidad de Años</small></p> 
                    </li>
                    <li class="buttons">
                        <input type="hidden" name="form_id" value="103675" />
                        <input id="saveForm" class="button_text" type="submit" name="submit" value="Calcular" />
                    </li>
		</ul>
		</form>	
	</div>
	<img id="bottom" src="bottom.png" alt="">
    </body>
</html>
