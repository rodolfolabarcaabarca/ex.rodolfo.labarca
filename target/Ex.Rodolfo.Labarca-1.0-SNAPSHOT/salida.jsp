<%-- 
    Document   : salida
    Created on : 29-03-2020, 17:06:52
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado Calculo Intereses</title>
        <link rel="stylesheet" type="text/css" href="view.css" media="all">
        <script type="text/javascript" src="view.js"></script>
    </head>
    </head>
    <body id="main_body">
        <%
            String capital = (String) request.getAttribute("capital"); 
            String intereses = (String) request.getAttribute("intereses");         
            String anhos = (String) request.getAttribute("anhos");
            String stotalIntereses = (String) request.getAttribute("totalIntereses");            
        %>
        <img id="top" src="top.png" alt="">
        <div id="form_container">
        <h1><a>Intereses</a></h1>
            <div class="form_description">
                <h2>Calculo de Intereses</h2>
                <p>Resultado</p>
            </div>
        <ul >
                    <li id="li_1" >
                        <label class="description" for="element_1">Con un capital de $<%=capital%>, con un interes <%=intereses%>% anual, y a <%=anhos%> años, los intereses correspondientes son: $<%=stotalIntereses%></label>
                    </li>
        </div>
        <img id="bottom" src="bottom.png" alt="">
    </body>
</html>
